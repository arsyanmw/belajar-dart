void main() {
  /**
   * list adalah array
   * list harus menentukan tipe data, jika tidak maka menjadi dynamic
   *
   * List<tipedata> namaVar = []
   * var namaVar = <tipedata>[]
   * final namaVar = <tipedata>[]
   *
   * list.add(value)      => menambah
   * list[index]          => get data
   * list[index] = value  => mengubah data pada index tertentu
   * list.removeAt[index] => menghapus data pada index tertentu dan menggeser data
   * list.length          => jumlah data
   */

  List<int> listInt = [];

  var listString = <String>[];

  print(listInt);
  print(listString);

  // add
  var names = <String>[
    'Areyan',
    'Mufti',
    'Wibowo'
  ];

  // names.add('Arsyan');
  // names.add('Mufti');
  // names.add('Wibowo');

  print(names);
  print(names.length);

  // get
  print(names[0]);

  // ubah
  names[1] = 'Ganteng';
  print(names[1]);

  // remove
  names.removeAt(1);
  print(names);
  print(names[1]);
}
