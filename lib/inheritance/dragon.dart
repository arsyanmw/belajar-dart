import 'package:belajar_dart/inheritance/monster.dart';

class Dragon extends Monster {
  String burn() => 'Dragon is burning you';

  // method dari parent abstract harus di override
  @override
  String move() {
    return 'Flying';
  }
}
