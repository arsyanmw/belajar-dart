void main() {
  /**
   * sama seperti List (array) tapi tidak dapat mengandung data yang duplikat/sama
   * menggunakan {}
   *
   * Set<tipedata> namaVar = {}
   * var namaVar = <tipedata>{}
   *
   * set.length         => jumlah data
   * set.add            => menambah data
   * set.remove(value)  => menghapus data
   */

  Set<int> num = {};
  var strings = <String>{};
  var doubles = <double>{};

  print(num);


  var names = <String>{
    'Arsyan',
    'Mufti',
    'Wibowo'
  };

  // add
  // names.add('Arsyan');
  // names.add('Arsyan');
  // names.add('Mufti');
  // names.add('Wibowo');

  print(names);
  print(names.length);

  //remove
  names.remove('Arsyan');
  names.remove('Budi');
  print(names);
  print(names.length);
}
