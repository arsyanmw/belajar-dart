void main() {
  /**
   * tipe data key pada map fleksibel
   *
   * Map<tipeKey, tipeValue> = {}
   *
   * map.length         => jumlah data
   * map[key]           => get data
   * map[key] = value   => ubah data
   * map.remove(key)    => menghapus data
   */

  Map<String, String> map1 = {};
  var map2 = Map<String, String>();
  var map3 = <String, String>{};

  print(map1);

  var names = <String, String>{
    'first' : 'Arsyan',
    'second' : 'Mufti',
    'third' : 'Wibowo',
  };

  // add
  // names['first'] = 'Arsyan';
  // names['second'] = 'Mufti';
  // names['third'] = 'Wibowo';

  // get
  print(names);
  print(names['first']);

  // ubah
  names['second'] = 'Ganteng';
  print(names['second']);

  // remove
  names.remove('second');
  print(names);
}
