void main() {
  // membuat variable dengan deklarasi tipe datanya
  // String name = "Arsyan Mufti Wibowo";

  // membuat variable dengan tipe data otomatis (var)
  var name  = "Arsyan Mufti Wibowo";

  print(name);
  print(name);
  print(name);
  print(name);


  //
  // final (variable yang tidak dapat dideklarasikan ulang/diubah keseluruhan nilainya)
  var firstName = "Budi";
  final lastName = "Santoso";

  firstName = "Cahyo";
  // lastName = "Pratama";

  print(firstName);
  print(lastName);


  //
  // const (variable yang tidak dapat diubah sama sekali)
  final array1 = [0,1,2];
  const array2 = [0,1,2];

  array1[0] = 10;
  // array2[0] = 6; //const ga bisa diubah

  print(array1);
  print(array2);


  //
  // late (variable yang akan muncul hanya ketika dipanggil (bukan ketika dideklarasikan))
  late var value = getValue();
  print("Variablenya mana?");
  print(value);
}

String getValue() {
  print("Fungsi Terpanggil");
  return "Ini Valuenya";
}
