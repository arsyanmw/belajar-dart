void sayHello({required String firstName, String midName = '', String lastName = ''}) {
  print('Hello $firstName $midName $lastName');
}

void main() {
  sayHello(firstName: 'Arsyan', midName: 'Mufti', lastName: 'Wibowo');
  sayHello(midName: 'Mufti', firstName: 'Arsyan');
  sayHello(firstName: 'Arsyan');
  sayHello(firstName: '');
}
