void main() async {
  var p = Person();

  print('job 1');
  print('job 2');
  p.getDataAsync().then((_) => {
    print('job 3 : ${p.name}')
  });
  print('job 4');
}

class Person {
  String name = 'no name';

  Future<void> getDataAsync() async {
    await Future.delayed(Duration(seconds: 3));
    name = 'Arsyan'; // misalnya ambil data dari database
    print('data berhasil di ambil');
  }
}
