void main() {
  int? age = null;
  age = 1;

  if(age != null) {
    double ageDouble = age.toDouble();

    print(ageDouble);
  }

  String name = 'Arsyan';
  String? nullName = name;

  int? nullNumber = null;
  if(nullNumber != null) {
    int price = nullNumber;
  }

  // default value
  String? guest;
  String guestName = guest ?? 'This Is Guest';

  print(guestName);

  /**
   *
   */
  int? nullableNumber;
  // nullableNumber = 10;
  int filledNumber = nullableNumber!;

  print(filledNumber);

  /**
   *
   */
  int? dataInt;
  double? dataDouble = dataInt?.toDouble();
}
