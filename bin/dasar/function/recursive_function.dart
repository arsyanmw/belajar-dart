int factorialLoop(int value) {
   var result = 1;
   for(var i = 1; i <= value; i++) {
     result *= i;
   }

   return result;
}

int factorialRecursive(int value) {
  if(value == 1) {
    return 1;
  } else {
    return value * factorialRecursive(value - 1);
  }
}

//stack overflow (error)
void loop(int value) {
  if(value == 0) {
    print('Done');
  } else {
    print('Loop ke-$value');
    loop(value - 1);
  }
}

void main() {
  print(factorialLoop(10));
  print(factorialRecursive(10));

  //stack overflow (error)
  loop(100000);
}
