/// this is main function
/// will executed by adart

void main() {
  // this is name variable
  var name = 'Arsyan';

  /**
   * this is multiline comment
   * you can add more line here
   */
  print(name);
}
