import 'package:belajar_dart/inheritance/monster.dart';

class Barbarian extends Monster {
  String punch() => 'Barbarian punch you';

  // method dari parent abstract harus di override
  @override
  String move() {
    return 'Walking';
  }
}
