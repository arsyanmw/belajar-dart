class PersegiPanjang {
  double? _panjang; // membuat private menggunakan underscore
  double? _lebar;

  void set lebar(double value) {
    if(value < 0) {
      value *= -1;
    }

    _lebar = value;
  }

  void setPanjang(double value) {
    if(value < 0) {
      value *= -1;
    }

    _panjang = value;
  }

  double get lebar {
    return _lebar!;
  }

  double getPanjang() => _panjang!;

  // kurang tepat
  double hitungLuas() => _panjang! * _lebar!;

  double get luas => _panjang! * _lebar!;
}
