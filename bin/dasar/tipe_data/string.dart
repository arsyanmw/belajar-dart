void main() {
  String firstName = 'Arsyan';
  String lastName = "Wibowo";

  print(firstName);
  print(lastName);

  /**
   * Interpolation = menambahkan string ke dalam string lain
   * menggunnakan tanda $
   */

  var fullname = '$firstName ${lastName}';

  print(fullname);

  /**
   * Karakter Backslash digunakan untuk menginisiasi bahwa karakter setelahnya
   * dapat di print di dalam string
   */

  var text = 'this \'dart\' is \$cool';
  print(text);

  /**
   * Menggabungkan string
   */

  var name1 = firstName + lastName;
  var name2 = 'Arsyan ' 'Wibowo';
  var name3 = 'Arsyan ' + lastName;

  print(name1);
  print(name2);
  print(name3);


  /**
   * Multiline = string yang panjang (dapat berbaris-baris)
   */

  var longString = '''
this is long string
multiline
in dart
  ''';

  print(longString);
}
