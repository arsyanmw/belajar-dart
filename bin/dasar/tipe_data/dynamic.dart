void main() {
  /**
   * dynamic = tipe data yang sewaktu-waktu dapat berubah
   */

  var contoh; // ini dynamic

  dynamic variable = 100;
  print(variable);

  variable = true;
  print(variable);

  variable = 'Anjay';
  print(variable);
}
