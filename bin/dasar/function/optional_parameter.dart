void sayHello(String firstName, [String midName = '', String lastName = '']) {
  print('Hello $firstName $midName $lastName');
}

void main() {
  sayHello('Arsyan', 'Mufti', 'Wibowo');
  sayHello('Arsyan', 'Mufti');
  sayHello('Arsyan');
}
