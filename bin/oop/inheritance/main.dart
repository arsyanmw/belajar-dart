import 'package:belajar_dart/inheritance/barbarian.dart';
import 'package:belajar_dart/inheritance/dragon.dart';
import 'package:belajar_dart/inheritance/hero.dart';
import 'package:belajar_dart/inheritance/monster.dart';

void main() async {
  Hero hero = Hero();
  // Monster mobs = Monster();
  Dragon dragon = Dragon();
  Barbarian barbarian = Barbarian();

  List<Monster> monsters = [];
  monsters.add(Dragon());
  monsters.add(Barbarian());

  // as => inisiasi Parent dari child yang mana
  // print((mobs as Dragon).burn());

  for (Monster m in monsters) {
    if(m is Dragon) {
      print(m.burn());
      print(m.move());
    } else if(m is Barbarian) {
      print(m.punch());
      print(m.move());
    } else {
      print(m.attackHuman());
    }
  }
}
