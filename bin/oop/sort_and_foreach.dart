void main() {
  List<Person> persons = [
    Person('Administrator', 32),
    Person('Moderator', 56),
    Person('Moderator', 21),
    Person('User', 33),
    Person('Administrator', 22),
    Person('User', 24),
    Person('Moderator', 29),
    Person('Administrator', 27),
    Person('User', 20),
  ];

  persons.sort((p1, p2) {
    if(p1.rolePoint - p2.rolePoint != 0) {
      return p1.rolePoint - p2.rolePoint;
    } else {
      return p1.age - p2.age;
    }
  });

  persons.forEach((element) {
    print('${element.role} - ${element.age}');
  });
}

class Person {
  String role;
  int age;

  Person(this.role, this.age);

  int get rolePoint {
    switch (role) {
      case 'Administrator':
        return 2;
      case 'Moderator':
        return 1;
      default:
        return 3;
    }
  }
}
