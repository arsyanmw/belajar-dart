void main() {
  var counter = 1;

  while(true) {
    print('Loop ke-$counter');
    counter++;

    if(counter > 10) {
      break;
    }
  }
}
