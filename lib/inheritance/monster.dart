import 'package:belajar_dart/inheritance/character.dart';

/**
 * abstract class tidak bisa dibuat menjadi objek
 * absract class tidak wajib memiliki method yang memiliki implentasi
 * method yang tidak memiliki implementasi harus di override di class child
 */
abstract class Monster extends Character{
  String attackHuman() => "Monster is attacking you";

  // absract class tidak wajib memiliki method yang memiliki implentasi
  // harus di overide
  String move();
}
