void main() {
  dynamic num = 100;

  var numInt = num as int;

  print(num);
  print(numInt);

  print(num is int);
  print(num is bool);
  print(num is! bool);
}
