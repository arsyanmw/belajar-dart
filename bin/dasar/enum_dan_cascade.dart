void main() {
  Mobs m = Mobs(status: MobsStatus.normal);
  print('Hello World');

  // cascade
  m
  ..getStatus()
  ..eat();
}

// enum
enum MobsStatus {normal, frozen, wet}

class Mobs {
  final MobsStatus status;

  Mobs({this.status = MobsStatus.normal});

  void getStatus() {
    switch(status) {
      case MobsStatus.normal:
        print('Mobs is daijoubu');
        break;
      case MobsStatus.frozen:
        print('Mobs is Frozen, can\'t moving');
        break;
      case MobsStatus.wet:
        print('Mobs is wet, frozen them!!');
        break;
      default:
    }
  }

  void eat() {
    print('Mobs eating apple..');
  }
}
