import 'dart:io';

void main() {
  int? number = int.tryParse(stdin.readLineSync()!);

  if (number! < 10 && number > 0) {
    print(number + 5);
  } else if (number > 10) {
    print(number + 10);
  } else if(number < 0) {
    print('Angka Negatif');
  } else {
    print(number);
  }
}
