void main() {
  var names = <String>['Arsyan', 'Mufti', 'Wibowo'];

  // for(var i = 0; i < names.length; i++) {
  //   print(names[i]);
  // }

  for(var val in names) {
    print(val);
  }

  var namesSet = <String>{'Arsyan', 'Mufti', 'Wibowo'};
  for(var v in namesSet) {
    print(v);
  }
}
