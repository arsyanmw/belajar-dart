int sum(List<int> numbers) {
  var total = 0;
  for(var val in numbers) {
    total += val;
  }

  return total;
}

String sayHello(String name) {
  return 'Halo $name';
}

void main() {
  var data = sayHello('Arsyan');
  print(sum([10, 10, 13, 7]));
  print(data);
}
