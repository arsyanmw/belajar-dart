void main() {

  /**
   * int = hanya return bilangan bulat
   * double = hanya return bilangan desimal
   * num = variabel yang sewaktu-waktu dapat berubah dari bulat ke desimal atau sebaliknya
   */

  int number1 = 10;
  double number2 = 10.5;
  num number3 = 15;
  
  number3 = 15.5;

  print(number1);
  print(number2);
  print(number3);
}
