void main() {
  var inpString = '1000';
  var inpInt = int.parse(inpString);
  var inpDouble = double.parse(inpString);

  print(inpString);
  print(inpInt);
  print(inpDouble);

  var intToDouble = inpInt.toDouble();
  var doubleToInt = inpDouble.toInt();

  print(intToDouble);
  print(doubleToInt);

  var intToString = inpInt.toString();
  var doubleToString = inpDouble.toString();

  print(intToString);
  print(doubleToString);
}
