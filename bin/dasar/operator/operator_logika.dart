void main() {
  var nilaiAkhir = 80;
  var nilaiAbsen = 50;

  var isAkhirGood = nilaiAkhir >= 75;
  var isAbsenGood = nilaiAbsen >= 75;

  print(isAkhirGood);
  print(isAbsenGood);

  // var lulus = isAkhirGood && isAbsenGood;
  var lulus = isAkhirGood || isAbsenGood;

  print(lulus);
  print(!true);
  print(!false);
}
