import 'package:belajar_dart/secure_box.dart';

void main() {
  var box = SecureBox<String>('ini rahasia', '123');
  var tgl = SecureBox<DateTime>(DateTime.now(), '677');

  print(box.getData('123').toString());
  print(tgl.getData('677').toString());
}
