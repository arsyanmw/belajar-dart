void sayHello(String name, String Function(String) filter) {
  print('Hello ${filter(name)}');
}

void main() {
  sayHello('Arsyan Wibowo', (String name) => name.toUpperCase());

  sayHello('Arsyan Wibowo', (name) {
    return name.toLowerCase();
  });

  var upperFunction = (String name) {
    return name.toUpperCase();
  };

  var lowerFunction = (String name) => name.toLowerCase();

  print(upperFunction('aRSyan'));
  print(lowerFunction('ARSyaN'));
}
