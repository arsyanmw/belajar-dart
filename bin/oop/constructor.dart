void main() {
  Person p = Person();

  print(p.name);
}

class Person {
  String? name;

  Person({String name = 'No Name'}) {
    this.name = name;
  }
}
