import 'dart:io';
import 'package:belajar_dart/encapsulation/class_persegipanjang.dart';

void main() {
  PersegiPanjang kotak1, kotak2;
  double luasKotak;

  kotak1 = PersegiPanjang();
  kotak1.setPanjang(-5);
  kotak1.lebar = 2;

  kotak2 = PersegiPanjang();
  print('Masukkan Panjang dan Lebar Kotak2 : ');
  kotak2.setPanjang(double.tryParse(stdin.readLineSync()!)!);
  kotak2.lebar = double.tryParse(stdin.readLineSync()!)!;

  luasKotak = kotak1.luas;

  print('Hasil hitung luas kotak1 dan kotak2');
  print('Luas Kotak 1 : $luasKotak');
  print('Luas Kotak 2 : ${kotak2.luas}');
  print('Panjang kotak1 : ${kotak1.getPanjang()}');
  print('Lebar kotak1 : ${kotak1.lebar}');
}
